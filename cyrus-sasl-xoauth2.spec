Name: cyrus-sasl-xoauth2
Version: 0.2
Release: 1%{?dist}
Summary: xoauth2 plugin for cyrus-sasl

License: MIT
URL: https://github.com/moriyoshi/cyrus-sasl-xoauth2
Source0: https://github.com/moriyoshi/cyrus-sasl-xoauth2/archive/v0.2.tar.gz

Patch1: 0001-fix-lib-dir-to-lib64.patch

BuildRequires:  cyrus-sasl-devel
BuildRequires:  automake
BuildRequires:  autoconf
BuildRequires:  libtool
Requires:       cyrus-sasl

%description
This is a plugin implementation of XOAUTH2.

FYI: if you are forced to use XOAUTH2-enabled SMTP / IMAP servers by your employer and want to keep using your favorite *nix MUA locally, the following detailed document should help a lot: http://mmogilvi.users.sourceforge.net/software/oauthbearer.html

%prep
%setup -q
%patch1 -p1
./autogen.sh

%build
%configure
make %{?_smp_mflags}

%install

rm -fr %{buildroot}
make install DESTDIR=%{buildroot}

rm -f %{buildroot}/%{_libdir}/sasl2/libxoauth2.a
rm -f %{buildroot}/%{_libdir}/sasl2/libxoauth2.la

%files
%{_libdir}/sasl2/
%{_libdir}/sasl2/libxoauth2.so
%{_libdir}/sasl2/libxoauth2.so.0
%{_libdir}/sasl2/libxoauth2.so.0.0.0

%changelog
